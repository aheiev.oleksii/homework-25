function Person(name, age) {
  this.name = name;
  this.age = age;

  this.render = function() {
    console.log(`Peson name: ${this.name}, person age: ${this.age}`);
  };
}

let oleksii = new Person('Oleksii', 30);
let karl = new Person(`Karl`, 15);

oleksii.render();
karl.render();

function Car(brand, model, yearOfRelease, id) {
  this.brand = brand;
  this.model = model;
  this.yearOfRelease = yearOfRelease;
  this.id = id;
  this.carOwner = function(owner) {
    owner.age >= 18 ? (this.owner = owner) : console.log(`${owner.name} has ${owner.age} he is too young`);
  };
  
  this.render = function() {
    let info = `Brand: ${this.brand}, model: ${this.model}, year of release: ${this.yearOfRelease}, id: ${this.id}`;
    
    if (this.owner !== undefined) {
      console.log(`${info}, owner: ${this.owner.name}`);
    } else {
      console.log(`${info}, owner: no owner`);
    }
  };
}

let wv = new Car('WV', 'B8', 2019, 123123123);
let  toyota = new Car('Toyota', 'Corolla', 2019, 2122122213);

wv.carOwner(oleksii);
wv.carOwner(karl);
toyota.carOwner(oleksii);
toyota.carOwner(karl);
wv.render();
toyota.render();